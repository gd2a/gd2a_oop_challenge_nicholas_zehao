/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop_challenge;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Nicholas Daryl
 * @author Ze Hao Shi
 */
public class OOP_Challenge {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //Challenge 5 - only the letters a-g are allowed and
        //there has to be at least one letter the regex
        //just has to match (not necessarily exactly)
        String challenge5 = "aaabcccccccccddefffg";
        String regExp = "[a-g]+";

        System.out.println("Challenge 5: " + challenge5.matches(regExp));

        //Challenge 6 - match challenge 5 exactly
        //has to be an exact match
        regExp = "aaabcccccccccddefffg";

        System.out.println("Challenge 6: " + challenge5.matches(regExp));
        //Challenge 7 - Match a string starting with a series of
        //letters followed by a period, after the period there
        //must be a series of digits
        //kjisl.22 would match but f5.12a would not
        String challenge7 = "abcd.135";
        regExp = "[a-zA-Z]+\\.[0-9]+";

        System.out.println("Challenge 7: " + challenge7.matches(regExp));

        //Challenge 8 - Modify 7 to use a group, so that we
        //print all digits that occur in a string that contain
        //multiple occurrences of the pattern
        String challenge8 = "abcd.135uvqz.7tzik.999";
        //You should print out 135, 7, 999
        regExp = "[a-zA-Z]+\\.[0-9]+";

        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(challenge8);

        System.out.print("challenge 8: ");

        while (matcher.find()) {
            String temp = matcher.group();
            System.out.print(temp.substring(temp.indexOf(".") + 1) + ", ");
        }

        /* Challenge 9: Suppose you are reading strings that match patterns
        used in challenges 7 and 8 from a file. Tabs are used
        to separate the matches with one exception. The last
        match is followed by a newline
         */
        String challenge9 = "abcd.135\tuvqz.7\ttzik.999\n";
        //Revise the regular expression and extract all the numbers
        regExp = "[a-zA-Z]+\\.[0-9]+";
        pattern = Pattern.compile(regExp);
        matcher = pattern.matcher(challenge9);

        System.out.print("\nChallenge 9: ");
        while (matcher.find()) {
            String temp = matcher.group();
            System.out.print(temp.substring(temp.indexOf(".") + 1) + ", ");
        }

        //Round 2
        /*Challenge 10 Instead of printing out the numbers
        print out their start and end indices. Use the same string as in 9. Make the indices inclusive e.g. start index printed for 135 would be 5 and the end should be 7
        Hint: Look at the documentation for Matcher.start() and Matcher.end()
         */
        pattern = Pattern.compile("(\\s)?[a-zA-Z]+\\.[0-9]+");
        matcher = pattern.matcher(challenge9);
        int count = 0;
        System.out.print("\nChallenge 10: ");
        while (matcher.find()) {
            count++;
            // find the difference between the length of the temp string and the index of the dot
            String temp = matcher.group();
            int dotIndex = (temp.length()-1) - (temp.indexOf(".") + 1);
            System.out.print("\nOccurence " + count + ": " + ((matcher.end() - 1) - dotIndex) + " to " + (matcher.end() - 1));
        }

        System.out.println("\nChallenge 11: ");
        String challenge11 = "{0, 2}, {0, 5}, {1, 3}, {2, 4}";
        String cha11 = "\\{([^}]*)\\}";
        regexChecker(cha11, challenge11);

        String challenge12 = "11112";
        String ch2regex = "[\\d]{5}";
        System.out.println("Challenge 12: " + challenge12.matches(ch2regex));

        String challenge13 = "11112-1234";
        String ch3regex = "[\\d]{5}[-][\\d]{4}";
        System.out.println("Challenge 13: " + challenge13.matches(ch3regex));
    }

    public static void regexChecker(String regExp, String toCheck) {
        Pattern checkerPattern = Pattern.compile(regExp);
        Matcher checkermatcher = checkerPattern.matcher(toCheck);
        while (checkermatcher.find()) {
            System.out.println(checkermatcher.group(1));
        }
    }

}
